/**
points(1, 1) ➞ 5
points(7, 5) ➞ 29
points(38, 8) ➞ 100
points(0, 1) ➞ 3
points(0, 0) ➞ 0
 */

import { points } from '.'

/**
 * Arg 1: Describes the test
 * Arg 2: Fn - Code for test
 */
it('should add a 1 two-pointer and a 1 three-pointer to equal 5', () => {
	// at least 1 assertion
	expect(points(1, 1)).toBe(5) // Assertion
})

it('should add a 7 two-pointers and a 5 three-pointer to equal 29', () => {
	// at least 1 assertion
	expect(points(7, 5)).toBe(29) // Assertion
})

it('should add a 38 two-pointers and a 8 three-pointer to equal 100', () => {
	// at least 1 assertion
	expect(points(38, 8)).toBe(100) // Assertion
})

it('should add a 0 two-pointers and a 1 three-pointer to equal 3', () => {
	// at least 1 assertion
	expect(points(0, 1)).toBe(3) // Assertion
})

it('should add a 0 two-pointers and a 0 three-pointer to equal 0', () => {
	// at least 1 assertion
	expect(points(0, 0)).toBe(0) // Assertion
})

it('should throw for negative two-pointers', () => {
	expect(() => points(-1, 1)).toThrow('Negative two-pointers')
})

it('should throw for negative three-pointers', () => {
	expect(() => points(1, -1)).toThrow('Negative three-pointers')
})
