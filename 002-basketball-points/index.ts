export const points = (twoPointers, threePointers) => {
	if (twoPointers < 0) {
		throw new Error('Negative two-pointers')
	}

	if (threePointers < 0) {
		throw new Error('Negative three-pointers')
	}

	return twoPointers * 2 + threePointers * 3
}
