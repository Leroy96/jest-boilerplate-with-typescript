/**
testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false
 */

import { testJackpot } from '.'

it('should return true if all elements are `@`', () => {
	expect(testJackpot(['@', '@', '@', '@'])).toBe(true)
})

it('should return true if all elements are `abc`', () => {
	expect(testJackpot(['abc', 'abc', 'abc', 'abc'])).toBe(true)
})

it('should return true if all elements are `SS`', () => {
	expect(testJackpot(['SS', 'SS', 'SS', 'SS'])).toBe(true)
})

it('should return false if all elements are not identical', () => {
	expect(testJackpot(['&&', '&', '&&&', '&&&&'])).toBe(false)
})

it('should return false if all elements are not identical', () => {
	expect(testJackpot(['SS', 'SS', 'SS', 'Ss'])).toBe(false)
})
