export const testJackpot = (array) => {
	// let firstSlot = array[0]
	// return array.every(function (slot) {
	// 	return slot === firstSlot
	// })

	return array.every(function (slot) {
		return slot === array[0]
	})
}

// Easy way
export function jackpot(array) {
	return new Set(array).size == 1
}
