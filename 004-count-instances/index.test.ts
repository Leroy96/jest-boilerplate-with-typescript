/** 
charCount("a", "edabit") ➞ 1
charCount("c", "Chamber of secrets") ➞ 1
charCount("B", "boxes are fun") ➞ 0
charCount("b", "big fat bubble") ➞ 4
charCount("e", "javascript is good") ➞ 0
charCount("!", "!easy!") ➞ 2
 */
import { charCount } from '.'

it('should return how many times "a" is found in "edabit"', () => {
	expect(charCount('a', 'edabit')).toBe(1)
})

it('should return how many times "c" is found in "Chamber of secrets"', () => {
	expect(charCount('c', 'Chamber of secrets')).toBe(1)
})

it('should return how many times "B" is found in "boxes are fun"', () => {
	expect(charCount('B', 'boxes are fun')).toBe(0)
})

it('should return how many times "b" is found in "big fat bubble"', () => {
	expect(charCount('b', 'big fat bubble')).toBe(4)
})

it('should return how many times "e" is found in "javascript is good"', () => {
	expect(charCount('e', 'javascript is good')).toBe(0)
})
it('should return how many times "!" is found in "!easy!"', () => {
	expect(charCount('!', '!easy!')).toBe(2)
})
