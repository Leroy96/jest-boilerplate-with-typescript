export const charCount = (character, word) => {
	return word.split(character).length - 1
}
