/*
splitOnDoubleLetter(‘Letter’) -> [‘let’, ‘ter’]
splitOnDoubleLetter(‘Really’) -> [‘real’, ‘ly’]
splitOnDoubleLetter(‘Happy’) -> [‘hap’, ‘py’]
splitOnDoubleLetter(‘Shall’) -> [‘shal’, ‘l’]
splitOnDoubleLetter(‘Tool’) -> [‘to’, ‘ol’]
splitOnDoubleLetter(‘Mississippi’) -> [‘mis’, ‘sis’, ‘sip’, ‘pi’]
splitOnDoubleLetter(‘Easy’) returns []

*/

import { splitOnDoubleLetter } from '.'

it('should split the string if there is a double letter', () => {
	expect(splitOnDoubleLetter('Letter')).toEqual(['let', 'ter'])
})

it('should split the string if there is a double letter', () => {
	expect(splitOnDoubleLetter('Really')).toEqual(['real', 'ly'])
})

// it('should split the string if there is a double letter', () => {
// 	expect(splitOnDoubleLetter('Happy')).toEqual(['hap', 'py'])
// })

// it('should split the string if there is a double letter', () => {
// 	expect(splitOnDoubleLetter('Shall')).toEqual(['shal', 'l'])
// })

// it('should split the string if there is a double letter', () => {
// 	expect(splitOnDoubleLetter('Tool')).toEqual(['to', 'ol'])
// })

// it('should split the string if there is a double letter', () => {
// 	expect(splitOnDoubleLetter('Mississippi')).toEqual(['mis', 'sis', 'sip', 'pi'])
// })

// it('should split the string if there is a double letter', () => {
// 	expect(splitOnDoubleLetter('Easy')).toEqual([])
// })
