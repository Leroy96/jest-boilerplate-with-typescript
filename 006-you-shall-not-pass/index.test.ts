import { checkPassword } from '.'

it('should check if password is valid', () => {
	expect(checkPassword('stonk')).toBe('Invalid')
})

it('should check if password is valid', () => {
	expect(checkPassword('pass word')).toBe('Invalid')
})

it('should check if password is valid', () => {
	expect(checkPassword('password')).toBe('Weak')
})

it('should check if password is valid', () => {
	expect(checkPassword('11081992')).toBe('Weak')
})

it('should check if password is valid', () => {
	expect(checkPassword('mySecurePass123')).toBe('Moderate')
})

it('should check if password is valid', () => {
	expect(checkPassword('!@!pass1')).toBe('Moderate')
})

it('should check if password is valid', () => {
	expect(checkPassword('@S3cur1ty')).toBe('Strong')
})
