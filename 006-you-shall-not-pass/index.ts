// Kevin Choi's solution
export function checkPassword(password) {
	// First check if password is valid
	if (password.search(/\s/) >= 0 || password.length < 6) return 'Invalid'

	// Now count how much criteria is met
	let criteriaMet = 0
	if (password.search(/[a-z]/) >= 0) criteriaMet++
	if (password.search(/[A-Z]/) >= 0) criteriaMet++
	if (password.search(/\d/) >= 0) criteriaMet++
	if (password.search(/\W/) >= 0) criteriaMet++
	if (password.length >= 8) criteriaMet++

	// Based on how much criteria met, return strength
	if (criteriaMet < 3) return 'Weak'
	if (criteriaMet < 5) return 'Moderate'
	return 'Strong'
}
