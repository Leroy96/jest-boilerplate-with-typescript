import { elasticize } from '.'

it('should elasticize ANNA to ANNNNA', () => {
	expect(elasticize('ANNA')).toBe('ANNNNA')
})

it('should elasticize KAYAK to KAAYYYAAK', () => {
	expect(elasticize('KAYAK')).toBe('KAAYYYAAK')
})

it('should elasticize X to X', () => {
	expect(elasticize('X')).toBe('X')
})
