export const elasticize = (word) => {
	let length = word.length
	if (length <= 3) {
		return word
	}

	let half = Math.floor(length / 2)

	let left = word.substr(0, half)
	let right = word.substr(length % 2 === 0 ? half : half + 1)

	let elasticizeLeft = ''
	let elasticizeRight = ''

	for (let i = 0, j = half - 1; i < half; i++, j--) {
		elasticizeLeft += Array.from({ length: i + 1 }, () => left[i]).join('')
		elasticizeRight += Array.from({ length: j + 1 }, () => right[i]).join('')
	}

	let elasticizeCenter = length % 2 === 0 ? '' : Array.from({ length: half + 1 }, () => word[half]).join('')

	return elasticizeLeft + elasticizeCenter + elasticizeRight
}
