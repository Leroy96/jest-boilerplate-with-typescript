import { noStrangers } from '.'

it('should', () => {
	expect(noStrangers('See Spot run. See Spot Jump. Spot likes jumping. See spot fly.')).toBe([['spot', 'see'], []])
})
