import { rearrange } from '.'

it('should rearrange the sentence bases on numbers', () => {
	expect(rearrange('is2 Thi1s T4est 3a')).toBe('This is a Test')
})

it('should rearrange the sentence bases on numbers', () => {
	expect(rearrange('4of Fo1r pe6ople 3good th5e the2')).toBe('For the good of the people')
})

it('should rearrange the sentence bases on numbers', () => {
	expect(rearrange('5weird i2s JavaScri1pt dam4n so3')).toBe('JavaScript is so damn weird')
})

it('should rearrange the sentence bases on numbers', () => {
	expect(rearrange('')).toBe('')
})
