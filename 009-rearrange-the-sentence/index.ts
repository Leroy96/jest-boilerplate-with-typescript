export const rearrange = (sentence) => {
	const newArray = []

	if (sentence === '') {
		return ''
	} else {
		// split sentence into array
		const words = sentence.split(' ')

		// search number in string and add to front of string
		for (const element of words) {
			for (let i = 0; i < words.length + 1; i++) {
				if (element.includes(i) === true) {
					const numberInString = element.match(/\d+/)[0]
					const numberToFront = numberInString + element
					newArray.push(numberToFront)
				}
			}
		}
		newArray.sort()
		const newString = newArray.join(' ').replace(/[0-9]/g, '')
		return newString
	}
}
