import { fiscalCode } from '.'

// fiscalCode({
// 	name: "Matt",
// 	surname: "Edabit",
// 	gender: "M",
// 	dob: "1/1/1900"
//    }) ➞ "DBTMTT00A01"
//    fiscalCode({
// 	name: "Helen",
// 	surname: "Yu",
// 	gender: "F",
// 	dob: "1/12/1950"
//    }) ➞ "YUXHLN50T41"
//    fiscalCode({
// 	name: "Mickey",
// 	surname: "Mouse",
// 	gender: "M",
// 	dob: "16/1/1928"
//    }) ➞ "MSOMKY28A16"

it('should return an 11 code character as a string.', () => {
	expect(fiscalCode({ name: 'Matt', surname: 'Edabit', gender: 'M', dob: '1/1/1900' })).toBe('DBTMTT00A01')
})

it('should return an 11 code character as a string.', () => {
	expect(fiscalCode({ name: 'Helen', surname: 'Yu', gender: 'F', dob: '1/12/1950' })).toBe('YUXHLN50T41')
})

it('should return an 11 code character as a string.', () => {
	expect(fiscalCode({ name: 'Mickey', surname: 'Mouse', gender: 'M', dob: '16/1/1928' })).toBe('MSOMKY28A16')
})
