export const fiscalCode = (personData) => {
	const months = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'H', 7: 'L', 8: 'M', 9: 'P', 10: 'R', 11: 'S', 12: 'T' }

	// Step 1: The surname
	let surnameCode: string
	const surnameCon = personData.surname.toUpperCase().match(/[B-DF-HJ-NP-TV-Z]/g) || []
	const surnameVow = personData.surname.toUpperCase().match(/[AEIOU]/g) || []

	if (personData.surname.length >= 3) {
		const together = [...surnameCon, ...surnameVow]
		surnameCode = together.slice(0, 3).join('')
	} else {
		surnameCode = surnameCon[0] + surnameVow[0] + 'X'
	}

	// Step 2: The first name
	let nameCode: string
	const nameCon = personData.name.toUpperCase().match(/[B-DF-HJ-NP-TV-Z]/g) || []
	const nameVow = personData.name.toUpperCase().match(/[AEIOU]/g) || []

	if (personData.name.length >= 3) {
		const together = [...nameCon, ...nameVow]

		if (nameCon.length > 3) {
			together.splice(1, 1)
		}

		nameCode = together.slice(0, 3).join('')
	} else {
		nameCode = nameCon[0] + nameVow[0] + 'X'
	}

	// Step 3: date and gender
	let finalPart: string
	const [day, month, year] = personData.dob.split('/')
	const yearCode = year.slice(2, 4)
	const monthCode = months[month]
	let dayCode: string
	if (personData.gender === 'M') {
		dayCode = parseInt(day) < 10 ? '0' + day : day
	} else {
		dayCode = (parseInt(day) + 40).toString()
	}
	finalPart = yearCode + monthCode + dayCode
	return [surnameCode, nameCode, finalPart].join('')
}
