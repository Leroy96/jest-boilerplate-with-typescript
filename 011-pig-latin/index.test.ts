import { pigLatinSentence } from '.'

it('should convert string to Pig Latin', () => {
	expect(pigLatinSentence('this is pig latin')).toBe('isthay isway igpay atinlay')
})

it('should convert string to Pig Latin', () => {
	expect(pigLatinSentence('wall street journal')).toBe('allway eetstray ournaljay')
})

it('should convert string to Pig Latin', () => {
	expect(pigLatinSentence('raise the bridge')).toBe('aiseray ethay idgebray')
})

it('should convert string to Pig Latin', () => {
	expect(pigLatinSentence('all pigs oink')).toBe('allway igspay oinkway')
})
