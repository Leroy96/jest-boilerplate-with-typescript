export const pigLatinSentence = (sentence) => {
	const sentenceArray = sentence.toLowerCase().split(' ')
	let result = []
	for (const word in sentenceArray) {
		const checkVowelIndex = sentenceArray[word].search(/[aeiou]/g)
		if (checkVowelIndex === 0) {
			const beginWithVowel = sentenceArray[word].concat('way')
			result.push(beginWithVowel)
		} else {
			const firstPart = sentenceArray[word].slice(0, checkVowelIndex)
			const lastPart = sentenceArray[word].slice(checkVowelIndex)
			const pigLatinWord = lastPart + firstPart + 'ay'
			result.push(pigLatinWord)
		}
	}
	return result.join(' ')
}
