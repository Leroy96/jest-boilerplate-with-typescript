import { humanReadable } from '.'

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(0)).toBe('00:00:00')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(59)).toBe('00:00:59')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(60)).toBe('00:01:00')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(90)).toBe('00:01:30')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(3599)).toBe('00:59:59')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(3600)).toBe('01:00:00')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(45296)).toBe('12:34:56')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(86399)).toBe('23:59:59')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(86400)).toBe('24:00:00')
})

it('should transform seconds to human readable time => 00:00:00', () => {
	expect(humanReadable(35999)).toBe('99:59:59')
})

// humanReadable(0) -> '00:00:00'
// humanReadable(59) -> '00:00:59'
// humanReadable(60) -> '00:01:00'
// humanReadable(90) -> '00:01:30'
// humanReadable(3599) -> '00:59:59'
// humanReadable(3600) -> '01:00:00'
// humanReadable(45296) -> '12:34:56'
// humanReadable(86399) -> '23:59:59'
// humanReadable(86400) -> '24:00:00'
// humanReadable(359999) -> '99:59:59
