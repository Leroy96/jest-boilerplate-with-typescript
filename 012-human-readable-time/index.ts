export const humanReadable = (input: number) => {
	let hours = Math.floor(input / 3600).toString()
	let minutes = Math.floor(Math.floor(input % 3600) / 60).toString()
	let seconds = Math.floor(Math.floor(input % 3600) % 60).toString()

	if (hours.length === 1) {
		hours = '0' + hours
	}
	if (minutes.length === 1) {
		minutes = '0' + minutes
	}
	if (seconds.length === 1) {
		seconds = '0' + seconds
	}
	const result = hours + ':' + minutes + ':' + seconds
	return result
	//  return 00:00:00
}
